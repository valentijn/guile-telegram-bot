;;;
;;; Copyright (C) 2017, Valentijn van de Beek
;;;
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public
;;; License along with this library; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;
;;; Author: Valentijn van de Beek <valentijn@posteo.net>
;;; Date: 16-06-2017
;;;
;;; Commentary:
;;; This file includes the methods for adding commands to the framework
;;;;;

(define-module (hyphan commands)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:use-module (oop goops)
  #:use-module (hyphan classes))

;; A rather low-tech solution for a command handler that probably needs to be rewritten.
;; It is probably everything but thread-safe.
(define commands '())

;; TODO: Do something more fancy if bot-command is set.
(define*-public (add-command keyword function #:optional #:key (bot-command #t))
  "Add a FUNCTION to the list of known functions for the command
handler which gets called if the message contains KEYWORD. If
BOT-COMMAND is set it will register it as a bot command and prepend a
forward slash"
  (if bot-command
      (set! commands (cons `(,(string-append "/" keyword) . ,function) commands))
      (set! commands (cons `(,keyword . ,function) commands))))

(define*-public (execute-command message #:optional #:key (search string-ci=?))
  "Find & execute the command associated with MESSAGE using string
search function SEARCH."
  (let ([entities (get-entities message)] ; Get the entities in the message
        ;; Search and try to execute the function in the command
        [func (lambda* (term #:optional (args '()))
                (let ([res (assoc term commands search)])
                  ;; TODO throw an error when it can't find the command
                  (cond [(and (null? args) res)
                         (catch 'wrong-number-of-args
                           (lambda () ((cdr res) message))
                           (lambda (key . args) ((cdr res) message '())))]
                        [res (catch 'wrong-number-of-args
                               (lambda () ((cdr res) message args))

                               (lambda (key . args) ((cdr res) message)))])))])

    ;; Check if the entities is empty.
    (if (null? entities)
        ;; Default to thinking it is not a bot-command if there aren't entities, probably wrong assumption
        ;; TODO: something more sensible instead.
        (func (get-text message))
        (let* ([entity (car entities)]
               [total (+ (get-offset entity) (get-length entity))])
          ;; Receive the command wherever in the message it may be.
          (func (substring (get-text message) (get-offset entity) (+ (get-offset entity) (get-length entity)))
                (if (= (string-length (get-text message)) total)
                    ""
                    (split-args (substring (get-text message) (+ total 1)))))))))

(define* (split-args str #:optional #:key (char #\Space) command)
  (letrec ([helper
            (lambda (args)
              (let ([match (string-match "=" (car args))])
                (if match
                    (cons (cons (match:prefix match) (match:suffix match))
                          (helper (cdr args)))
                    (cons (cons "rest"
                                (substring (fold (lambda (x y) (string-append y " " x)) "" args) 1))
                          '()))))])
    (helper (string-split str char))))
