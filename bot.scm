(define-module (hyphan bot)
  #:use-module (ice-9 optargs)

  #:use-module (oop goops)
  #:use-module (hyphan classes)
  #:re-export (display write)

  ;; Internal modules
  #:use-module (hyphan commands)
  #:use-module (hyphan functions)
  #:use-module (hyphan telegram))


(define get-update
  (let ([last-offset 0])
    (lambda ()
      (define jsobj (telegram-get-updates #:offset (+ last-offset 1)))
      (if (null? jsobj)
          jsobj
          (let ([update (make <telegram-update> #:jsobj jsobj)])
            (set! last-offset (get-id update))
            update)))))


(define-public (process-update)
  (let ([update (get-update)])
    (unless (null? update)
      (let ([message (or (get-message update) (get-edited-message update))])
        (display message)
        (execute-command message)))
    (run-current-event!)))

(define*-public (respond-to-message message text #:optional reply)
  (if reply
      (telegram-send-message (get-id (get-chat message)) text #:reply-to-message-id (get-id message))
      (telegram-send-message (get-id (get-chat message)) text)))
