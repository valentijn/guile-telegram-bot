
(define-module (hyphan telegram)
  ;;; Modules to connect to Telegram and unmangle their response
  #:use-module (rnrs bytevectors)
  #:use-module (curl)

  ;; More define variations
  #:use-module (ice-9 optargs)  ; define*
  #:use-module (ice-9 format)   ; format

  ;; future ice-9 json
  ;; jsobj-ref, `(@)
  #:use-module (sjson)
  #:use-module (sjson utils))

;; The bot API key
;; TODO: change to a class?
(define bot-key "EXAMPLE")
(define handle (curl-easy-init))

;; Set the bot API key to str
(define-public (set-bot-key str)
  (set! bot-key str))

;; Server you want to communicate with
(define telegram-server "https://api.telegram.org/bot")

(define* (telegram-send command #:optional body)
  "Takes COMMAND, a string, which indicates the command you want to
send and BODY, a list of symbols, strings and numbers, which is the
body you want to sent with the command."
  (if body
      (begin (curl-easy-setopt handle 'post #t)
             (curl-easy-setopt handle 'httpheader '("Content-Type: application/json"
                                                    "Charset: utf-8"
                                                    "X-Clank-Overhead: GNU Terry Pratchett"))
             (curl-easy-setopt handle 'postfields (string->utf8 (write-json-to-string body))))
      (begin (curl-easy-setopt handle 'httpheader '("Charset: utf-8"
                                                    "X-Clank-Overhead: GNU Terry Pratchett"))
             (curl-easy-setopt handle 'post #f)))

  (curl-easy-setopt handle 'url (string-append telegram-server bot-key "/" command))

  (let ([result (read-json-from-string (curl-easy-perform handle))])
    ;; Return result if the command succeeded else throw an error.
    (if (jsobj-ref result "ok")
        (jsobj-ref result "result")
        (error (string-append
                "[" (number->string (jsobj-ref result "error_code")) "] "
                (jsobj-ref result "description"))))))

(define-public (telegram-get-me)
  "Send the getMe command to Telegram"
  (telegram-send "getMe"))

;;; Add to the json objects
(define-syntax-rule (set-arg! obj name value) (set! obj (jsobj-set obj name value)))

(define*-public (telegram-send-message chat-id text
                                       #:optional #:key
                                       disable-notification?
                                       disable-web-page-preview?
                                       reply-to-message-id)
  "Send a message to the chat with to CHAT-ID (number) with the text
TEXT (string). Disable a preview image of the web page if
WEB-PAGE-PREVIEW is set, disable the sending of a notification to the
people/person in the chat if DISABLE-NOTIFICATIONS is set and reply to
the message with the id REPLY-TO-MESSAGE-ID."
  (let ([jsobj `(@ ("chat_id" ,chat-id)
                   ("text" ,text)
                   ("disable_notifications" ,disable-notification?)
                   ("disable_web_page_preview" ,disable-web-page-preview?))])
    (set-arg! jsobj "reply_to_message_id" reply-to-message-id)
    (telegram-send "sendMessage" jsobj)))

;; TODO: Set default offset to -1?
;; TODO: If limit is 1 send just the jsobj and not an array?
;; TODO: Fix allowed-updates which doesn't work in an uri query string
(define*-public (telegram-get-updates #:optional #:key offset limit timeout allowed-updates)
  "Return an list with 'jsobj's which represent the messages that the
bot has achieved. It will optionally mark all message before the
message with the id OFFSET if, only fetch LIMIT number of messages,
use a timeout of TIMEOUT and/or only allow updates of the type
indicated with ALLOWED-UPDATES (a list of strings)"
  (let ([jsobj `(@)])
    (set-arg! jsobj "offset" offset)
    (set-arg! jsobj "limit" limit)
    (set-arg! jsobj "timeout" timeout)
    (set-arg! jsobj "allowed_updates" allowed-updates)

    (telegram-send "getUpdates" jsobj)))
