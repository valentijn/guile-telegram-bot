;;;
;;; Copyright (C) 2017, Valentijn van de Beek
;;;
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public
;;; License along with this library; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;
;;; Author: Valentijn van de Beek <valentijn@posteo.net>
;;; Date: 16-06-2017
;;;
;;; Commentary:
;;; This file defines functions to create functions that will run every few
;;; minutes.
;;;;;
(define-module (hyphan functions)
  #:use-module (ice-9 optargs)

  ;; Telegram classes and their generics
  #:use-module (oop goops)      ; make
  #:use-module (hyphan classes) ; everything
  #:use-module (hyphan telegram)
  #:re-export (display write)   ; re-export so we don't lose the base functions

  #:use-module (srfi srfi-19))


(define insert-event! #f)
(define find-event #f)
(define-public print-events #f)
(define remove-event! #f)

(let ([events '()])
  (set! insert-event!
        (lambda (utc-date event)
          (set! events (acons utc-date event events))))
  (set! find-event
        (lambda (utc-date)
          (assoc-ref events utc-date)))
  (set! remove-event!
        (lambda (utc-date)
          ;; I must be doing something wrong...
          (set! events (assoc-remove! events utc-date))))

  (set! print-events (lambda () events)))

(define* (insert-event-date! event year month day #:optional (hour 0) (minute 0) (offset 0))
  "Insert an EVENT, a thunk, under the date using the numbers YEAR, MONTH, DAY, HOUR, MINUTE and OFFSET."
  (insert-event! (date->time-utc (make-date 0 0 minute hour day month year offset))
                 event))
(define* (find-event-date year month day #:optional (hour 0) (minute 0) (offset 0))
  "Find & returned the event stored under the date YEAR, MONTH, DAY, HOUR, MINUTE and OFFSET"
  (find-event
   (date->time-utc (make-date 0 0 minute hour day month year offset))))

(define*-public (run-current-event!)
  "Find the event that is scheduled to run in this minute."
  (let* ([base (current-date 0)]
         [date (make-date 0 0 (date-minute base) (date-hour base) (date-day base) (date-month base) (date-year base) 0)]
         [utc (date->time-utc date)]
         [event (find-event utc)])
    (when event
      (remove-event! utc)
      (event))))

(define (add-to-current-time year month day hour minute)
  "Get the current time and add YEAR, MONTH, DAY, HOUR and MINUTE to it."
  (let* ([base (current-date 0)])
    (make-date 0 0
               (+ (date-minute base) minute)
               (+ (date-hour base) hour)
               (+ (date-day base) day)
               (+ (date-month base) month)
               (+ (date-year base) year)
               0)))

(define*-public (add-reminder message args)
  (define (get-arg name)
    (let ([result (assoc-ref args name)])
      (if result (string->number result) 0)))

  (insert-event!
   (date->time-utc
    (add-to-current-time
     (get-arg "year")
     (get-arg "month")
     (get-arg "day")
     (get-arg "hour")
     (get-arg "minute")))
   (lambda () (telegram-send-message (get-id (get-chat message))
                             (assoc-ref args "rest")
                             #:reply-to-message-id (get-id message))))

  (telegram-send-message (get-id (get-chat message)) "Reminder set."))
