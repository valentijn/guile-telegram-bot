;;;
;;; Copyright (C) 2017, Valentijn van de Beek
;;;
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public
;;; License along with this library; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;
;;; Author: Valentijn van de Beek <valentijn@posteo.net>
;;; Date: 16-06-2017
;;;
;;; Commentary:
;;; This file defines the functions to communicate with Telegram and a basic
;;; example of a Telegram bot in action.
;;;;;

(define-module (hyphan main)
  ;; More define variations
  #:use-module (ice-9 optargs)  ; define*

  ;; Telegram classes and their generics
  #:use-module (oop goops)      ; make
  #:use-module (hyphan classes) ; everything
  #:re-export (display write)   ; re-export so we don't lose the base functions

  ;; Internal modules
  #:use-module (hyphan commands)
  #:use-module (hyphan functions)
  #:use-module (hyphan bot)

  ;; Commands
  #:use-module (ice-9 regex)    ; regexp-replace/global
  #:use-module (ptpb ptpb)

  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19))    ; map, fold

(define (replace-with-b-emoticon message args)
  "An example function that takes a telegram message MESSAGE and
change all first letters of the words, letter c & letter c to the
letter b."
  (let* ([text (substring (get-text message) 2)]
         [b-emoticon "🅱"]
         [pass-1 (regexp-substitute/global #f "[b-c]" text 'pre b-emoticon 'post)]
         [pass-2 (regexp-substitute/global #f "([A-Za-z]3{1})([^ ].)" pass-1 'pre b-emoticon 2 'post)])
    (respond-to-message message pass-2 #t)))
(add-command "b" replace-with-b-emoticon)

;; (define (create message args)
;;   (respond-to-message
;;    (if (assoc-ref args "language")
;;        (string-append (jsobj-ref (create-paste (assoc-ref args "rest")) "url")
;;                       "/"
;;                       (assoc-ref args "language")))
;;    #t))

;; (define (shorten-url message args)
;;   (respond-to-message message (jsobj-ref (short-url (car args)) #t)))

;;(add-command "create" create)
(add-command "addreminder" add-reminder)

;; (add-command "shorten" shorten-url)

;; Add a function which utilizes some more Telegram classes features to the list using a lambda
(add-command "hello"
             (lambda (message)
               (respond-to-message message
                                   (string-append "Hello to you too " (get-first-name (get-from message)) "!")
                                   #t)))


;; Add a message handler to the list.
(add-command "test"
             (lambda (message)
               (respond-to-message message "Yes, I am awake!"))
             #:bot-command #f)


;; The last seen offset.
(while #t
  (process-update)
  (sleep 1))
