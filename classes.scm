;;;
;;; Copyright (C) 2017, Valentijn van de Beek
;;;
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
;;; 02110-1301 USA
;;;
;;; Author: Valentijn van de Beek <valentijn@posteo.net>
;;; Date: 16-06-2017
;;;
;;; Commentary:
;;; This file defines and exports the classes for converting the Telegram json
;;; objects to something that Guile can properly read. It also defines some
;;; generic functions for writing a representation of these classes for ease of
;;; logging, developing and debugging.
;;;
;;;;;

(define-module (hyphan classes)
  #:use-module (oop goops)
  #:use-module (sjson)
  #:use-module (sjson utils)

  #:export (;; Functions
            time-format-string
            set-time-format

            <telegram-user>
            get-id get-username get-first-name get-last-name get-language-code

            <telegram-chat>
            get-type get-photo get-description get-invite-link get-all-members-are-administrators

            <telegram-message-entity>
            get-offset get-length get-url get-user

            ;; The monster class that is message.
            <telegram-message>
            get-chat get-from get-date get-text get-edit-date get-reply-to-message get-entities
            get-forward-from get-from-date get-forward-from-chat get-forward-from-message-id
            get-audio get-video get-document get-game get-sticker get-voice get-video-note
            get-new-chat-members get-caption get-contact get-venue get-new-chat-member
            get-left-chat-member get-new-chat-title get-new-chat-photo get-delete-chat-photo
            get-group-chat-created get-supergroup-chat-created get-migrate-to-chat-id
            get-migrate-from-chat-id get-pinned-message get-invoice get-successful-payment

            <telegram-update>
            get-message get-edited-message channel-post edited-channel-post inline-query
            chosen-inline-result callback-query shipping-query pre-checkout-query))


(define time-format-string "%d/%m - %H:%M")
(define (set-time-format format)
  (set! time-format-string format))

;;;
;;; Telegram-user class
;;;
(define-class <telegram-user> ()
  (jsobj         #:init-keyword #:jsobj #:getter get-jsobj)
  (id            #:getter get-id)
  (first-name    #:getter get-first-name)
  (last-name     #:getter get-last-name)
  (username      #:getter get-username)
  (language-code #:getter get-language-code))

(define-method (initialize (user <telegram-user>) initargs)
  (let ([jsobj (cadr initargs)])
    (slot-set! user 'id            (jsobj-ref jsobj "id"))
    (slot-set! user 'first-name    (jsobj-ref jsobj "first_name"))
    (slot-set! user 'last-name     (jsobj-ref jsobj "last_name"))
    (slot-set! user 'username      (jsobj-ref jsobj "username"))
    (slot-set! user 'language-code (jsobj-ref jsobj "language_code"))
    (next-method)))

;; Print a nice looking representation of the class for use of logging.
;; For a more pratical representation one should use write instead.
;; TODO: make the respresentation configurable.
(define-method (display (self <telegram-user>) port)
  (format port
          "~A~A (@~A ~A)~&"
          (get-first-name self)
          (if (get-last-name self) (string-append " " (get-last-name)) "")
          (if (get-username self) (get-username self) "")
          (get-id self)))

;; Note: write is also used internally for display functions since they are smaller
;; Please never end them with a newline.
(define-method (write (self <telegram-user>) port)
  (format port
          "~A~A (@~A)"
          (get-first-name self)
          (if (get-last-name self) (string-append " " (get-last-name)) "")
          (if (get-username self) (get-username self) "")))

(define-class <telegram-chat> ()
  (jsobj       #:init-keyword #:jsobj #:getter get-jsobj)
  (id          #:getter get-id)
  (type        #:getter get-type)
  (title       #:getter get-title)
  (username    #:getter get-username )
  (first-name  #:getter get-first-name)
  (last-name   #:getter get-last-name)
  (photo       #:getter get-photo)
  (description #:getter get-description)
  (invite-link #:getter get-invite-link)
  (all-members-are-administrators #:getter get-all-members-are-administrators))

(define-method (initialize (chat <telegram-chat>) initargs)
  (let ([jsobj (cadr initargs)])
    (slot-set! chat 'id          (jsobj-ref jsobj "id"))
    (slot-set! chat 'type        (jsobj-ref jsobj "type"))
    (slot-set! chat 'title       (jsobj-ref jsobj "title"))
    (slot-set! chat 'username    (jsobj-ref jsobj "username"))
    (slot-set! chat 'first-name  (jsobj-ref jsobj "first_name"))
    (slot-set! chat 'last-name   (jsobj-ref jsobj "last_name"))
    (slot-set! chat 'description (jsobj-ref jsobj "description"))
    (slot-set! chat 'invite-link (jsobj-ref jsobj "invite_link"))
    (slot-set! chat 'all-members-are-administrators
               (jsobj-ref jsobj "all_members_are_administrators"))))

(define-method (display (self <telegram-chat>) port)
  (if (string=? (get-type self) "group")
      (format port "[~A] ~A (~A)~&" (get-type self) (get-title self) (get-id self))
      (format port "[~A] ~A~A (@~A ~A)~&"
              (get-type self)
              (get-first-name self)
              (if (get-last-name self) (string-append " " (get-last-name)) "")
              (if (get-username self) (get-username self) "")
              (get-id self))))

(define-class <telegram-message-entity> ()
  (jsobj  #:init-keyword #:jsobj #:getter get-jsobj)
  (type   #:getter get-type)
  (offset #:getter get-offset)
  (length #:getter get-length)
  (url    #:getter get-url)
  (user   #:getter get-user))

(define-method (initialize (message-entity <telegram-message-entity>) initargs)
  (let ([jsobj (cadr initargs)])
    (slot-set! message-entity 'type   (jsobj-ref jsobj "type"))
    (slot-set! message-entity 'offset (jsobj-ref jsobj "offset"))
    (slot-set! message-entity 'length (jsobj-ref jsobj "length"))
    (slot-set! message-entity 'url    (jsobj-ref jsobj "url"))

    ;; jsobj-ref will return #f when it can't find the right object, when
    ;; this happens it will throw an error when you pass it to make. This
    ;; is caught and the the entity is set to #f.
    (slot-set! message-entity 'user
               (catch 'match-error
                 (lambda () (make <telegram-user> #:jsobj (jsobj-ref jsobj "user")))
                 (lambda (key . args) #f)))))

(define-class <telegram-message> ()
  (jsobj                   #:init-keyword #:jsobj  #:getter get-jsobj)
  (id                      #:getter get-id)
  (from                    #:getter get-from)
  (date                    #:getter get-date)
  (chat                    #:getter get-chat)
  (forward-from            #:getter get-forward-from)
  (forward-from-chat       #:getter get-forward-from-chat)
  (forward-from-message-id #:getter get-forward-from-message-id)
  (forward-date            #:getter get-forward-date)
  (reply-to-message        #:getter get-reply-to-message)
  (edit-date               #:getter get-edit-date)
  (text                    #:getter get-text)
  (entities                #:getter get-entities)
  (audio                   #:getter get-audio)
  (document                #:getter get-document)
  (game                    #:getter get-game)
  (photo                   #:getter get-photo)
  (sticker                 #:getter get-sticker)
  (video                   #:getter get-video)
  (voice                   #:getter get-voice)
  (video_note              #:getter get-video-note)
  (new-chat-members        #:getter get-new-chat-members)
  (caption                 #:getter get-caption)
  (contact                 #:getter get-contact)
  (location                #:getter get-location)
  (venue                   #:getter get-venue)
  (new-chat-member         #:getter get-new-chat-member)
  (left-chat-member        #:getter get-left-chat-member)
  (new-chat-title          #:getter get-new-chat-title)
  (new-chat-photo          #:getter get-new-chat-photo)
  (delete-chat-photo       #:getter get-delete-chat-photo)
  (group-chat-created      #:getter get-group-chat-created)
  (supergroup-chat-created #:getter get-supergroup-chat-created)
  (channel-chat-created    #:getter get-channel-chat-created)
  (migrate-to-chat-id      #:getter get-migrate-to-chat-id)
  (migrate-from-chat-id    #:getter get-migrate-from-chat-id)
  (pinned-message          #:getter get-pinned-message)
  (invoice                 #:getter get-invoice)
  (successful-payment      #:getter get-successful-payment))

;; Basic initialize
;; TODO expand this further
(define-method (initialize (message <telegram-message>) initargs)
  (let* ([jsobj (cadr initargs)]
         [catch-make (lambda (class type name)
                       (catch 'match-error
                         (lambda () (slot-set! message type (make class #:jsobj (jsobj-ref jsobj name))))
                         (lambda (key . args) (slot-set! message type #f))))])
    (slot-set! message 'id (jsobj-ref jsobj "message_id"))
    (catch-make <telegram-user> 'from "from")
    (catch-make <telegram-chat> 'chat "chat")

    ;; Set the entities slot to an array filled with telegram entities, since it is an array
    ;; we can't use catch-make and instead do basically the same thing but use '() as a fallback.
    ;; IMPORTANT: (if entities) will not work use (if (null? entities)), both should possibly be accepted.

    (slot-set! message 'entities
               (map (lambda (entity) (make <telegram-message-entity> #:jsobj entity))
                  (let [(entities (jsobj-ref jsobj "entities"))]
                    (if entities entities '()))))

    (slot-set! message 'date (jsobj-ref jsobj "date"))
    (slot-set! message 'text (jsobj-ref jsobj "text"))))

(define-method (display (self <telegram-message>) port)
  (let ([chat (get-chat self)])
    (format port
            "[~A] ~S (~S) by "
            (strftime time-format-string (localtime (get-date self)))
            (get-text self)
            (get-id self))

    ;; Write the user
    (write (get-from self))

    (when (string=? (get-type chat) "group")
      (format port
              " in ~A (~A)"
              (get-title chat)
              (get-id chat)))

    (format port "~&")))

(define-class <telegram-update> ()
  (jsobj                #:init-keyword #:jsobj #:getter get-jsobj)
  (id                   #:getter get-id)
  (message              #:getter get-message)
  (edited-message       #:getter get-edited-message)
  (channel-post         #:getter get-channel-post)
  (edited-channel-post  #:getter get-edited-channel-post)
  ;; Not implemented from here!
  (inline-query         #:getter get-inline-query)
  (chosen-inline-result #:getter get-chosen-inline-result)
  (callback-query       #:getter get-callback-query)
  (shipping-query       #:getter get-shipping-query)
  (pre-checkout-query   #:getter get-pre-checkout-query))

(define-method (initialize (update <telegram-update>) initargs)
  (let* ([jsobj (caadr initargs)]
         [catch-make (lambda (class type name)
                       (catch 'match-error
                         (lambda () (slot-set! update type (make class #:jsobj (jsobj-ref jsobj name))))
                         (lambda (key . args) (slot-set! update type #f))))])
    (slot-set! update 'id (jsobj-ref jsobj "update_id"))
    (catch-make <telegram-message> 'message             "message")
    (catch-make <telegram-message> 'edited-message      "edited_message")
    (catch-make <telegram-message> 'channel-post        "channel_post")
    (catch-make <telegram-message> 'edited-channel-post "edited_channel_post")
    (next-method)))
